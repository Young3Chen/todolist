### O: use navigate with router and mockApi to complete toDoListItem and add css style
### R: useful 
### I: 
1. use router can make a page just start loading finished, redux data will not change, so the jump between pages can be more silky smooth, bringing a better user experience, but at the same time may also bring the first time to load the slow problem!
2. mockApi allows us to better focus on some of the front-end logic and page writing, mock of the fake data can also be casually tested, is a very convenient way to provide tests.
### D:Want to write a good front-end UI is also need to accumulate time and experience, a good component division can also solve a lot of style and data on the proble and I hope to handle it someday.