import { createSlice } from '@reduxjs/toolkit';

export const todoList = createSlice({
  name: 'todoList',
  initialState: {
    todoList: [],
  },
  reducers: {
    loadToDoList: (state, action) => {
      state.todoList = action.payload
    },
  },
})

export const { loadToDoList } = todoList.actions

export default todoList.reducer