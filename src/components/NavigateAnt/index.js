import React, { useState } from 'react';
import { FileDoneOutlined, QuestionOutlined, HomeOutlined } from '@ant-design/icons';
import { Menu } from 'antd';
import { Link } from 'react-router-dom';
const items = [
  {
    label: (
      <Link to="/"> Home Page </Link>
    ),
    key: 'home',
    icon: <HomeOutlined />,
  },
  {
    label: (
      <Link to="/doneList"> Done Page </Link>
    ),
    key: 'doneList',
    icon: <FileDoneOutlined />,
  },
  {
    label: (
      <Link to="/help"> Help Page </Link>
    ),
    key: 'help',
    icon: <QuestionOutlined />,
  },
];
const App = () => {
  const [current, setCurrent] = useState('mail');
  const onClick = (e) => {
    setCurrent(e.key);
  };
  return <Menu onClick={onClick} selectedKeys={[current]} mode="horizontal" items={items} />;
};
export default App;