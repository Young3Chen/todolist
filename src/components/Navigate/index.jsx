import { Link } from "react-router-dom"
import "./index.css"

const Navigate = () => {
    return (
        <div className="navigate-position">
            <ul>
                <li> <Link to="/"> Home Page </Link>  </li>
                <li> <Link to="/doneList"> Done Page </Link>  </li>
                <li> <Link to="/help"> Help Page </Link>  </li>
            </ul>
        </div>
    )
}
export default Navigate 