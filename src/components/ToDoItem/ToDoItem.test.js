import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import MyComponent from './MyComponent';
import ToDoItem from "."

const mockStore = configureStore([]);
const initialState = {};

describe('MyComponent', () => {
    beforeEach(() => {
        store = mockStore(initialState);
    });
    test('renders correctly', () => {
        const { getByText } = render(
            <Provider store={store}>
                <MyComponent />
            </Provider>
        );
        expect(getByText('Hello World')).toBeInTheDocument();
    });
    test('dispatches an action on button click', () => {
        const { getByRole } = render(
            <Provider store={store}>
                <MyComponent />
            </Provider>
        );
        fireEvent.click(getByRole('button'));
        const actions = store.getActions();
        expect(actions).toEqual([{ type: 'INCREMENT' }]);
    });
});




// const mockStore = configure([])
// const

// describe("ToDoItem test", () => {
//     it("shoule render default text value with text1", ()=>{
//         render(<ToDoItem toDoStr={{id: "123",text:"text1",done:false}}></ToDoItem>)
//         expect(screen.getByText(/"text1"/i)).toBeInTheDocument()
//     })
// })