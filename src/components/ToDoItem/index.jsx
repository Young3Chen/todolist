import { CloseOutlined, EditOutlined } from '@ant-design/icons';
import { Input, Modal } from 'antd';
import React, { useState } from 'react';
import useToDo from "../../hooks/useToDo";
import "./index.css";

const ToDoItem = ({ itemProject }) => {
    const { deleteToDoItem, updateToDoItem } = useToDo()
    const handleDoneToDo = async () => {
        deleteToDoItem(itemProject.id)
    }
    const handleUpdateToDo = async () => {
        updateToDoItem(itemProject.id, !itemProject.done, itemProject.text)
    }
    const [open, setOpen] = useState(false);
    const [confirmLoading, setConfirmLoading] = useState(false);
    const [modalText, setModalText] = useState('');
    const showModal = () => {
        setModalText(itemProject.text)
        setOpen(true);
    };
    const handleOk = async() => {
        if (modalText.trim() !== "") {
            setConfirmLoading(true)
            await updateToDoItem(itemProject.id, itemProject.done, modalText)
            setOpen(false)
            setConfirmLoading(false)

        }
    };
    const handleModalTextChange = (e) => {
        setModalText(e.target.value)
    }
    const handleCancel = () => {
        setOpen(false);
    };
    return (
        <li className="item-style">
            <div
                title={itemProject.text}
                onClick={handleUpdateToDo}
                className={itemProject.done ? "item-text item-done" : "item-text"}>
                {itemProject.text}
            </div>
            <EditOutlined onClick={showModal} className="item-delete" />
            <CloseOutlined onClick={handleDoneToDo} className="item-delete" />
            <Modal
                title="Edit"
                open={open}
                onOk={handleOk}
                confirmLoading={confirmLoading}
                onCancel={handleCancel}
            >
                <div><Input value={modalText} onChange={handleModalTextChange} /></div>
            </Modal>
        </li>

    )
}
export default ToDoItem