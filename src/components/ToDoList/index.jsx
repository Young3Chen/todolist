import { useEffect } from "react"
import useToDo from "../../hooks/useToDo"
import ToDoGenerator from "../ToDoGenerator"
import ToDoGroup from "../ToDoGroup"
import "./index.css"
const TodoList = () => {
    const { loadAllToDoList } = useToDo()
    useEffect(() => {
        loadAllToDoList()
    }, [])
    return (
        <div className="box">
            <span className="font-style">Todo List</span>
            <ToDoGroup ></ToDoGroup>
            <ToDoGenerator></ToDoGenerator>
        </div>
    )
}

export default TodoList