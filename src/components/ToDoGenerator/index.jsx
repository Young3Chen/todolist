import { useState } from "react"
import useToDo from "../../hooks/useToDo"
import { AppstoreAddOutlined } from '@ant-design/icons';
import { Input,Button } from 'antd';
import "./index.css"

const ToDoGenerator = () => {
    const [inputStr, setInputStr] = useState("");
    const { createToDoItem } = useToDo()
    const handleChange = (e) => {
        setInputStr(e.target.value);
    }
    const handleNewItem = async () => {
        if (inputStr.trim() !== "") {
            createToDoItem(inputStr)
        }
        setInputStr("")
    }
    return (
        <div className="generator">
            <Input size="large" onChange={handleChange} value={inputStr} className="input-style"></Input>
            <Button type="primary" size="large" className="button-style" ghost icon={<AppstoreAddOutlined />} onClick={handleNewItem}> 
                add
            </Button>
        </div>
    )
}
export default ToDoGenerator