import { useSelector } from "react-redux"
import ToDoItem from "../ToDoItem"
import "./index.css"
const ToDoGroup = () => {
    const todoList = useSelector((state) => state.todoList.todoList)
    return (
        <div className="todo-list-container">
                {
                    todoList.map((value, index) => <ToDoItem itemProject={value} key={index}></ToDoItem>)
                }
        </div>

    )
}
export default ToDoGroup
