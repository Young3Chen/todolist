import { useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import "./index.css"

const DoneListPage = () => {
    const doneTodoList = useSelector((state) => state.todoList.todoList).filter(todo => todo.done)
    const navigate = useNavigate()
    const handleClickDetail = (id) => {
        navigate(`/doneList/${id}`)
    }
    return (
        <div className="done">
             <span className="title">Done List</span>
            {
                doneTodoList.map(
                    doneTodo =>
                        <div className="done-item" key={doneTodo.id} onClick={() => handleClickDetail(doneTodo.id)} >
                            {doneTodo.text}
                        </div>
                )
            }
        </div>
    )
}

export default DoneListPage