import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getTodoItemById } from "../../../apis/api";
import "./index.css";

const DoneListDetail = () => {
    const { id } = useParams();
    const [doneDetail, setDoneDetail] = useState("");
    const fetchDoneDetail = useCallback(async () => {
        const response = await getTodoItemById(id);
        setDoneDetail(response);
    }, [id]);  

    useEffect(() => {
        fetchDoneDetail();
    }, [fetchDoneDetail]);  
    return (
        <div className="done-list">
            <div> DoneDetail </div>
            <div> id:  {doneDetail.id} </div>
            <div> text: {doneDetail.text}</div>
        </div>
    );
};

export default DoneListDetail;