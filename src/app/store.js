import { configureStore } from '@reduxjs/toolkit'
import toDoListSlice from '../ToDoList/toDoListSlice'

export default configureStore({
  reducer: {
    todoList: toDoListSlice
  },
})