import { useDispatch } from "react-redux"
import { loadToDoList } from "../ToDoList/toDoListSlice"
import { todoApis } from "../apis/Todo"

const useToDo = () => {

    const dispatch = useDispatch()

    const loadAllToDoList = async () => {
        const { data } = await todoApis.getTodos()
        dispatch(loadToDoList(data))
    }
    const createToDoItem = async (text) => {
        await todoApis.addTodo(text)
        loadAllToDoList()
    }
    const updateToDoItem = async (id, done, text) => {
        await todoApis.updateTodo(id, done, text)
        loadAllToDoList()
    }
    const deleteToDoItem = async (id) => {
        await todoApis.deleteTodo(id)
        loadAllToDoList()
    }

    return {
        loadAllToDoList,
        createToDoItem,
        updateToDoItem,
        deleteToDoItem
    }
}

export default useToDo