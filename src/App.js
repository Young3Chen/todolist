import { Outlet } from 'react-router-dom';
import './App.css';
import Navigate from './components/NavigateAnt';

function App() {
  return (
    <div className="App">
        <Navigate></Navigate>
        <Outlet></Outlet>
    </div>
  );
}

export default App;
