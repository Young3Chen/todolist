import axios from "axios";

const instance = axios.create({
    baseURL: 'http://localhost:8080'
})


export const addTodo = (text) => {
    return instance.post(`/todos`, {
        done: false,
        text
    })
}

export const getTodos = () => {
    return instance.get('/todos')
}

export const deleteTodo = (id) => {
    return instance.delete(`/todos/${id}`)
}

export const updateTodo = (id, done, text) => {
    return instance.put(`/todos/${id}`, { done, text })
}

export const getTodoItemById = (id) => {
    return instance.get(`/todos/${id}`)
}

export const todoApis = { addTodo, getTodos, deleteTodo, updateTodo, getTodoItemById }
