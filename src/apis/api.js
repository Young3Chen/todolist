import axios from "axios";
import { Modal  } from 'antd';

const instance = axios.create({
    baseURL: 'http://localhost:8080'
})

instance.interceptors.response.use((response) => {
    return response.data;
}, (error) => {

    if (error.response.status === 404) {
        Modal.error({
            title: 'Error',
            content: "Not Found",
        });
    }
    return Promise.reject(error);
});

export const getTodos = () => {
    return instance.get('/todos')
}

export const addTodo = (text) => {
    return instance.post(`/todos`, {
        done: false,
        text
    })
}

export const deleteTodo = (id) => {
    return instance.delete(`/todos/${id}`)
}

export const updateTodo = (id, done, text) => {
    return instance.put(`/todos/${id}`, { done, text })
}

export const getTodoItemById = (id) => {
    return instance.get(`/todos/${id}`)
}


export const todoApis = { addTodo, getTodos, deleteTodo, updateTodo, getTodoItemById }