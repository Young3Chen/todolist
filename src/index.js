import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import store from './app/store'
import { Provider } from 'react-redux'
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import HelpPage from './app/pages/HelpPages';
import NotFoundPage from './app/pages/NotFoundPage';
import DoneListPage from './app/pages/DoneListPage';
import TodoList from './components/ToDoList';
import DoneListDetail from './app/pages/DoneListDetail';


const root = ReactDOM.createRoot(document.getElementById('root'));
const router = createBrowserRouter([
  {
    path: '/',
    element: <App></App>,
    children: [
      {
        index: true,
        element: <TodoList></TodoList>
      },
      {
        path: '/help',
        element: <HelpPage></HelpPage>
      },
      {
        path: '/doneList',
        element: <DoneListPage></DoneListPage>
      },
      {
        path: '/doneList/:id',
        element: <DoneListDetail></DoneListDetail>
      }
    ]
  },

  {
    path: '/*',
    element: <NotFoundPage></NotFoundPage>
  }

])


root.render(
  <Provider store={store}>
    <RouterProvider router={router} />
  </Provider>
);
reportWebVitals();
